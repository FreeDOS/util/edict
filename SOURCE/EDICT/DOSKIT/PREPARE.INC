; Copyright (C) 2018 Jerome Shidel
;
;   This program is free software; you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation; either version 2 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License along
;   with this program; if not, write to the Free Software Foundation, Inc.,
;   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

; NASM 2.14rc0 for DOS

; jump to START is not needed, all code is in macros and no assembly
; instructions are compiled prior to START label.
; jmp START

; Set Program Data Area Macro and validate PDA_START is declared
%idefine PDA(x) PDA_START + tPDA. %+ x

; -----------------------------------------------------------------------------
; Common Constant Defines
; -----------------------------------------------------------------------------

%idefine CR         0x0d
%idefine LF         0x0a
%idefine CRLF       CR, LF

%idefine            LowerCaseHexidecimal

; -----------------------------------------------------------------------------
; Begin code Segment and enable forward macro references
; -----------------------------------------------------------------------------

; SECTION _TEXT

%idefine CODE_STAGE BLOCK_FORWARD

%include 'COMMON.INC'

; -----------------------------------------------------------------------------
; Main program start point
; -----------------------------------------------------------------------------

START:
    ; Initialize Zero the Program Data Area, Initialize Buffered Standard I/O
    ; File Handles and Adjust the Stack
    RAM_Prepare



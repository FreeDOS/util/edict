; Copyright (C) 2018 Jerome Shidel
;
;   This program is free software; you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation; either version 2 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License along
;   with this program; if not, write to the Free Software Foundation, Inc.,
;   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

; NASM 2.14rc0 for DOS

; -----------------------------------------------------------------------------
; FORWARD
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_FORWARD

; Buffered File I/O

%imacro FileBufferRead 3
    %idefine REQUIRE_FileBufferRead
    mpush       dx, bx, cx
    mov         bx, %1
    mov         dx, %2
    mov         cx, %3
    call        FUNCTION_FileBufferRead
    mpop        dx, bx, cx
%endmacro

%imacro FileBufferWrite 3
    %idefine REQUIRE_FileBufferWrite
    %idefine REQUIRE_FileBufferFlush
    mpush       bx, dx, cx
    mov         bx, %1
    mov         dx, %2
    mov         cx, %3
    call        FUNCTION_FileBufferWrite
    mpop        bx, dx, cx
%endmacro

%imacro FileBufferFlush 1
    %idefine REQUIRE_FileBufferFlush
    push        bx
    mov         bx, %1
    call        FUNCTION_FileBufferFlush
    pop         bx
%endmacro

%endif

; -----------------------------------------------------------------------------
; CODE - Forward macro support code
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_CODE
; Buffered File I/O

%ifdef REQUIRE_FileBufferRead
FUNCTION_FileBufferRead:
    clc
    mpush       si, di, bx, cx, dx
    mov         si, bx
    mov         di, dx

    mov         bx, [si + tIOBUFFER.PTR]
    mov         dx, [si + tIOBUFFER.CNT]
    push        cx
.Transfer:
    cmp         cx, 0
    je          .Done
    cmp         dx, bx
    je          .ReadFile
    mov         al, [si + tIOBUFFER.BUF + bx]
    mov         [di], al
    inc         bx
    inc         di
    dec         cx
    jmp         .Transfer
.ReadFile:
    push        cx
    mov         dx, si
    add         dx, tIOBUFFER.BUF
    OS_FileRead [si + tIOBUFFER.HDL], dx, SIZEOF_IOBUF
    pop         cx
    mov         dx, ax
    xor         bx, bx
    jc          .Error
    cmp         dx, 0
    je          .EOF       ; end of file
    jmp         .Transfer
.Error:
    stc
.EOF:
    xor         ax, ax
.Done:
    mov         [si + tIOBUFFER.PTR], bx
    mov         [si + tIOBUFFER.CNT], dx

    pop         bx                  ; pushed cx
    jc          .ErrorCode
    mov         ax, bx
    sub         ax, cx
.ErrorCode:

    mpop        si, di, bx, cx, dx
    ret
%endif

%ifdef REQUIRE_FileBufferFlush
FUNCTION_FileBufferFlush:
    clc
    mpush       si, bx, cx, dx
    mov         si, bx
    mov         cx, [si + tIOBUFFER.CNT]
    cmp         cx, 0
    je          .Empty
    mov         bx, [si + tIOBUFFER.HDL]
    mov         dx, si
    add         dx, tIOBUFFER.BUF
    OS_FileWrite bx, dx, cx
.Empty:
    xor         cx, cx
    mov         [si + tIOBUFFER.CNT], cx
    mpop        si, bx, cx, dx
    ret
%endif

%ifdef REQUIRE_FileBufferWrite
FUNCTION_FileBufferWrite:
    clc
    mpush       si, di, bx, cx, dx
    mov         si, bx
    mov         di, dx

    mov         bx, [si + tIOBUFFER.CNT]
    push        cx
.Transfer:
    cmp         cx, 0
    je          .Done
    cmp         bx, SIZEOF_IOBUF
    je          .WriteFile
    mov         al, [di]
    mov         [si + tIOBUFFER.BUF + bx], al
    inc         bx
    inc         di
    dec         cx
    jmp         .Transfer
.WriteFile:
    mov         [si + tIOBUFFER.CNT], bx
    mov         bx, si
    call        FUNCTION_FileBufferFlush
    xor         bx, bx
    cmp         ax, SIZEOF_IOBUF
    jne         .Error
    jc          .Error
    jmp         .Transfer
.Error:
    stc
.Done:
    mov         [si + tIOBUFFER.PTR], bx
    mov         [si + tIOBUFFER.CNT], bx

    pop         bx                  ; pushed cx
    jc          .ErrorCode
    mov         ax, bx
    sub         ax, cx
.ErrorCode:

    mpop        si, di, bx, cx, dx
    ret
%endif

%endif

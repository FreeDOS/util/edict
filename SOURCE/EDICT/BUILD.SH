#!/bin/bash

[[ -d source/edict ]] && cd source/edict

[[ -e ../../bin/edict.com ]] && rm ../../bin/edict.com
[[ -e ../../edict.zip ]] && rm ../../edict.zip

nasm edict.asm -Idoskit/ -fbin -O9 -o ../../bin/edict.com || exit 1

cd ../..

chmod +x bin/edict.com
zip -q -9 -k -r edict.zip appinfo bin doc nls source

ls -al bin/edict.com edict.zip